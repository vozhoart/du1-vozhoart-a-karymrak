package ts1.fel.cvut.cz;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArticleSearchTest {
    WebDriver driver;
    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        Dimension dimension = new Dimension(1920, 1080); // Ширина и высота в пикселях
        driver.manage().window().setSize(dimension);
        driver.get("https://link.springer.com/");
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    //parametrizované testy pomocí vytvořených Page Object Modelů,
    // které Vás přihlásí jako uživatele a vyhledávají uložené články (pomocí jejich názvu)
    // a zkontrolují, že odpovídá jejich datum publikace a DOI.
    @ParameterizedTest
    @CsvFileSource(files = "articles.csv")
    public void testArticleDetails(String expectedName, String expectedHref, String expectedDate) {

        SpringerHomePage springerHomePage = new SpringerHomePage(driver);

        //Вход в аккаунт
        springerHomePage.acceptCookiesIfPresent();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        SpringerLoginPage springerLoginPage = springerHomePage.clickLoginLink();
        springerLoginPage.acceptCookiesIfPresent();

        springerLoginPage.enterEmail("vozhov.artem1@gmail.com");

        springerLoginPage.clickContinueButton();

        springerLoginPage.enterPassword("SNAPSHOT12!2");

        springerLoginPage.submitPassword();

        driver.get("https://link.springer.com/search");
        SpringerSearchPage searchPage = new SpringerSearchPage(driver);
        searchPage.enterSearchQuery(expectedName);
        searchPage.submitSearch();

        wait.until(driver -> searchPage.getFirstArticleName() != null);


        String actualName = searchPage.getFirstArticleName();
        String actualHref = searchPage.getFirstArticleDOI();
        String actualDate = searchPage.getFirstArticleDate();

        assertEquals(expectedName, actualName);
        assertEquals(expectedHref, actualHref);
        assertEquals(expectedDate, actualDate);

        System.out.println("Expected name: " + expectedName + "\nGoten: " + actualName);
    }
}
