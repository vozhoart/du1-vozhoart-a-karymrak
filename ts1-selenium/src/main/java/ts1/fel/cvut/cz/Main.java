package ts1.fel.cvut.cz;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws IOException {

//                SNAPSHOT12!2
//                SNAPSHOT12!2
        FileWriter writer = null;


        //dr
        WebDriver driver = new ChromeDriver();
        Dimension dimension = new Dimension(1920, 1080); // Ширина и высота в пикселях
        driver.manage().window().setSize(dimension);
        driver.get("https://link.springer.com/");
        SpringerHomePage springerHomePage = new SpringerHomePage(driver);



        //Вход в аккаунт
        springerHomePage.acceptCookiesIfPresent();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        SpringerLoginPage springerLoginPage = springerHomePage.clickLoginLink();
        springerLoginPage.acceptCookiesIfPresent();

        springerLoginPage.enterEmail("vozhov.artem1@gmail.com");

        springerLoginPage.clickContinueButton();

        springerLoginPage.enterPassword("SNAPSHOT12!2");

        springerLoginPage.submitPassword();



        //Поиск  všechny články(Content type = Article) obsahující slova “Page Object Model”
        // a alespoň jedním ze slov “Sellenium Testing” publikovaných v tomto roce.



        driver.get("https://link.springer.com/advanced-search");
        SpringerAdvancedSearchPage springerAdvancedSearchPage = new SpringerAdvancedSearchPage(driver);

//        springerAdvancedSearchPage.acceptCookiesIfPresent();
        new WebDriverWait(driver, Duration.ofSeconds(10));

        springerAdvancedSearchPage.enterAllWords("Page Object Model");
        new WebDriverWait(driver, Duration.ofSeconds(10));

        springerAdvancedSearchPage.enterLeastWords("Sellenium Testing");
        new WebDriverWait(driver, Duration.ofSeconds(10));


//        springerAdvancedSearchPage.selectDateFacetMode("in");
//        new WebDriverWait(driver, Duration.ofSeconds(10));

        //почему advanced search не работает как надо с годом публикации. Даже если делать всё вручную(( год надо устанавливать в SearchPage
        springerAdvancedSearchPage.enterFacetStartYear("2024");
        new WebDriverWait(driver, Duration.ofSeconds(10));
        springerAdvancedSearchPage.enterFacetEndYear("2024");


        SpringerSearchPage springerSearchPage = springerAdvancedSearchPage.submitSearch();

        wait.until(ExpectedConditions.urlContains("search"));


        springerSearchPage.selectDatePublishedCustom("2024", "2024");


        springerSearchPage.selectContentTypeArticle();
        springerSearchPage.submitSearch();

// Initialize FileWriter
        writer = new FileWriter("articles.csv");
//        writer.append("Name, Href, Publish Date\n");

        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));

        // Find all <li> elements within the <ol> element that represent search result items
        List<WebElement> items = list.findElements(By.cssSelector("li[data-test='search-result-item']"));

        // Loop through the first four items and extract the required details
        for (int i = 0; i < 4; i++) {
            WebElement item = items.get(i);

            // Extract the href attribute from the <a> element within the <h3> tag
            WebElement link = item.findElement(By.cssSelector("h3.app-card-open__heading > a"));
            String href = link.getAttribute("href");

            // Extract the article name from the <span> element within the <a> tag
            String articleName = link.findElement(By.tagName("span")).getText();

            // Extract the publishing date from the <span> element with data-test="published"
            String publishDate = item.findElement(By.cssSelector("span[data-test='published']")).getText();

            writer.append(String.format("\"%s\", \"%s\", \"%s\"\n", articleName, href, publishDate));

            // Print the extracted details
            System.out.println("Article " + (i + 1) + ":");
            System.out.println("Name: " + articleName);
            System.out.println("Href: " + href);
            System.out.println("Publish Date: " + publishDate);
            System.out.println();

            writer.flush();
        }

        driver.quit();
    }


}