package ts1.fel.cvut.cz;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SpringerHomePage extends SpringerPage{



    @FindBy(css = "a[data-test='springerlink-logo']")
    private WebElement logoLink;

    @FindBy(id = "identity-account-widget")
    private WebElement loginLink;

    public SpringerHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickLogo() {
        logoLink.click();
    }

    public SpringerLoginPage clickLoginLink() {
        loginLink.click();
        return new SpringerLoginPage(driver);
    }
}
