package ts1.fel.cvut.cz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SpringerLoginPage extends SpringerPage{

    @FindBy(id = "login-email")
    private WebElement emailInput;

    @FindBy(id = "email-submit")
    public WebElement continueButton;


    @FindBy(id = "login-password")
    private WebElement passwordInput;

    @FindBy(id = "password-submit")
    private WebElement passwordSubmitButton;

    public SpringerLoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void clickContinueButton() {
        wait.until(ExpectedConditions.elementToBeClickable(continueButton));
        continueButton.click();
    }

    public void enterPassword(String password) {
        wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
        passwordInput.sendKeys(password);
    }

    public void submitPassword() {
        wait.until(ExpectedConditions.elementToBeClickable(passwordSubmitButton));
        passwordSubmitButton.click();
    }

}
