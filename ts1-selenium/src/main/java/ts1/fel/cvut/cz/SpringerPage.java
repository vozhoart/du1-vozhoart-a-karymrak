package ts1.fel.cvut.cz;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class SpringerPage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    public void acceptCookiesIfPresent() {
        try {
            By acceptCookiesButtonLocator = By.cssSelector("button[data-cc-action='accept']");
            WebElement acceptCookiesButton = wait.until(ExpectedConditions.elementToBeClickable(acceptCookiesButtonLocator));
            acceptCookiesButton.click();
        } catch (Exception e) {
            // Tlačítko "Accept all cookies" se nezobrazilo, pokračujte dále
        }
    }
    public SpringerPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }
    protected void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }


}
