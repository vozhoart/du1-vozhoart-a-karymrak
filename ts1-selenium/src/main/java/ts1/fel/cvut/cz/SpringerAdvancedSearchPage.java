package ts1.fel.cvut.cz;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpringerAdvancedSearchPage extends SpringerPage{


    @FindBy(id = "all-words")
    private WebElement allWordsInput;

    @FindBy(id = "exact-phrase")
    private WebElement exactPhraseInput;

    @FindBy(id = "least-words")
    private WebElement leastWordsInput;

    @FindBy(id = "without-words")
    private WebElement withoutWordsInput;

    @FindBy(id = "title-is")
    private WebElement titleIsInput;

    @FindBy(id = "author-is")
    private WebElement authorIsInput;

    @FindBy(id = "date-facet-mode")
    private WebElement dateFacetModeSelect;

    @FindBy(id = "facet-start-year")
    private WebElement facetStartYearInput;

    @FindBy(id = "facet-end-year")
    private WebElement facetEndYearInput;

    @FindBy(id = "results-only-access-checkbox-advanced")
    private WebElement showLockedResultsCheckbox;

    @FindBy(id = "submit-advanced-search")
    private WebElement submitButton;

    public SpringerAdvancedSearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }



    public void enterAllWords(String words) {
        waitForElementToBeClickable(allWordsInput);
        allWordsInput.sendKeys(words);
    }


    public void enterLeastWords(String words) {
        waitForElementToBeClickable(leastWordsInput);
        leastWordsInput.sendKeys(words);
    }



    public void selectDateFacetMode(String mode) {
        waitForElementToBeClickable(dateFacetModeSelect);
        dateFacetModeSelect.sendKeys(mode);
    }

    public void enterFacetStartYear(String year) {
        waitForElementToBeClickable(facetStartYearInput);
        facetStartYearInput.sendKeys(year);
    }
    public void enterFacetEndYear(String year) {
        waitForElementToBeClickable(facetEndYearInput);
        facetEndYearInput.sendKeys(year);
    }

    public void checkShowLockedResults(boolean check) {
        waitForElementToBeClickable(showLockedResultsCheckbox);
        if (check!= showLockedResultsCheckbox.isSelected()) {
            showLockedResultsCheckbox.click();
        }
    }

    public SpringerSearchPage submitSearch() {
        waitForElementToBeClickable(submitButton);
        submitButton.click();
        return new SpringerSearchPage(driver);
    }
}
