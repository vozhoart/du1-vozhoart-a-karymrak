package ts1.fel.cvut.cz;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SpringerSearchPage extends SpringerPage {

    @FindBy(id = "search-springerlink")
    private WebElement searchInput;

    @FindBy(id = "search-submit")
    private WebElement searchSubmitButton;


    @FindBy(id = "popup-filters")
    private WebElement filtersPopup;


    @FindBy (id = "content-type-article")
    private WebElement contentTypeArticleCheckbox;


    @FindBy(id = "date-custom")
    private WebElement customDateCheckbox;

    @FindBy(id = "date-from")
    private WebElement dateFromInput;

    @FindBy(id = "date-to")
    private WebElement dateToInput;

    public SpringerSearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterSearchQuery(String query) {
        waitForElementToBeClickable(searchInput);
        searchInput.sendKeys(query);
    }



    public void selectContentTypeArticle() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", contentTypeArticleCheckbox);

    }


    public String getFirstArticleName() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("h3.app-card-open__heading > a > span")).getText();
    }

    public String getFirstArticleDOI() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("h3.app-card-open__heading > a")).getAttribute("href");
    }

    public String getFirstArticleDate() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("span[data-test='published']")).getText();
    }



//    public void selectDatePublishedCustom(String from, String to) {
//        waitForElementToBeClickable(dateFromInput);
//        dateFromInput.sendKeys(from);
//        waitForElementToBeClickable(dateToInput);
//        dateToInput.sendKeys(to);
//
//    }



    public void selectDatePublishedCustom(String from, String to) {
        // Прокрутка элемента dateFromInput в область видимости
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dateFromInput);
        // Ожидание, пока элемент станет кликабельным
        waitForElementToBeClickable(dateFromInput);
        // Ввод данных в dateFromInput
        dateFromInput.sendKeys(from);

        // Прокрутка элемента dateToInput в область видимости
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dateToInput);
        // Ожидание, пока элемент станет кликабельным
        waitForElementToBeClickable(dateToInput);
        // Ввод данных в dateToInput
        dateToInput.sendKeys(to);
    }



    public void submitSearch() {
        waitForElementToBeClickable(searchSubmitButton);
        searchSubmitButton.click();
    }
}
