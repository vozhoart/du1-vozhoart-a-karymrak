package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap();
        ArrayList<Order> orderArchive  = new ArrayList();

        Order test1 = new Order(new ShoppingCart(),"car","KONST");
        orderArchive.add(test1);
        itemPurchaseArchive.put(1,new ItemPurchaseArchiveEntry(new Item(1,"car",543,"toy") {
        }));
        itemPurchaseArchive.put(2,new ItemPurchaseArchiveEntry(new Item(2,"car2",566,"toy") {
        }));

        PurchasesArchive test = new PurchasesArchive(itemPurchaseArchive,orderArchive);
        System.out.println(test.getHowManyTimesHasBeenItemSold(new Item(1,"car",543,"toy"){}));
        test.putOrderToPurchasesArchive(test1);
    }
}
