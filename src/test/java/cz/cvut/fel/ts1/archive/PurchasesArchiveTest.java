package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PurchasesArchiveTest {
    StandardItem item1;
    StandardItem item2;
    StandardItem item3;
    ShoppingCart shoppingCart;
    private Order orderArchiveMock;


    private ItemPurchaseArchiveEntry entry1Mock;
    private ItemPurchaseArchiveEntry entry2Mock;

    @BeforeEach
    void setUp() {
        item1 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
        item2 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
        item3 = new StandardItem(2, "Item2", 20.0f, "Category2", 200);
        shoppingCart = new ShoppingCart();

        // Mock the OrderArchive
        orderArchiveMock = mock(Order.class);

        // Mock the ItemPurchaseArchiveEntry
        entry1Mock = mock(ItemPurchaseArchiveEntry.class);
        entry2Mock = mock(ItemPurchaseArchiveEntry.class);
    }


    @org.junit.jupiter.api.Test
    void getHowManyTimesHasBeenItemSold() {

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();

        ItemPurchaseArchiveEntry entry1 = spy(new ItemPurchaseArchiveEntry(item1));
        ItemPurchaseArchiveEntry entry2 = spy(new ItemPurchaseArchiveEntry(item2));

        entry1.increaseCountHowManyTimesHasBeenSold(5);
        entry2.increaseCountHowManyTimesHasBeenSold(8);

        itemPurchaseArchiveMock.put(1, entry1);
        itemPurchaseArchiveMock.put(2, entry2);

        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, null);



        assertEquals(5, archive.getHowManyTimesHasBeenItemSold(item1));
        assertEquals(8, archive.getHowManyTimesHasBeenItemSold(item2));
        assertEquals(0, archive.getHowManyTimesHasBeenItemSold(item3));
    }

    @Test
    public void testPutOrderToPurchasesArchive() {

        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);

        ArrayList<Item> orderItems = new ArrayList<>();
        orderItems.add(item1);
        orderItems.add(item2);
        Order order = new Order(shoppingCart, "Bob Alobl", "Melka 666");
        order.setItems(orderItems);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        ItemPurchaseArchiveEntry entry1 = spy(new ItemPurchaseArchiveEntry(item1));
        ItemPurchaseArchiveEntry entry2 = spy(new ItemPurchaseArchiveEntry(item2));

        itemPurchaseArchiveMock.put(1, entry1);
        itemPurchaseArchiveMock.put(2, entry2);

        ArrayList<Order> orderArchiveMock = spy(new ArrayList<>());
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, orderArchiveMock);

        archive.putOrderToPurchasesArchive(order);

        assertEquals(1, archive.orderArchive.size());
        assertEquals(order, archive.orderArchive.get(0));

        assertEquals(1, archive.itemPurchaseArchive.get(1).getCountHowManyTimesHasBeenSold());
        assertEquals(1, archive.itemPurchaseArchive.get(2).getCountHowManyTimesHasBeenSold());


        verify(orderArchiveMock).add(order);
        verify(entry1).increaseCountHowManyTimesHasBeenSold(1);
        verify(entry2).increaseCountHowManyTimesHasBeenSold(1);
    }


    @Test
    public void testPrintItemPurchaseStatistics() throws IOException {
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        ItemPurchaseArchiveEntry entry1 = spy(new ItemPurchaseArchiveEntry(item1));
        ItemPurchaseArchiveEntry entry2 = spy(new ItemPurchaseArchiveEntry(item3));


        entry1.increaseCountHowManyTimesHasBeenSold(5);
        entry2.increaseCountHowManyTimesHasBeenSold(10);

        itemPurchaseArchiveMock.put(1, entry1);
        itemPurchaseArchiveMock.put(2, entry2);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        try {
            PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, null);
            archive.printItemPurchaseStatistics();

            String expectedOutput = "ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                    "ITEM  Item   ID 1   NAME Item1   CATEGORY Category1   PRICE 10.0   LOYALTY POINTS 100   HAS BEEN SOLD 6 TIMES" + System.lineSeparator() +
                    "ITEM  Item   ID 2   NAME Item2   CATEGORY Category1   PRICE 20.0   LOYALTY POINTS 200   HAS BEEN SOLD 10 TIMES" + System.lineSeparator();
            assertEquals(expectedOutput, outContent.toString());
        } finally {
            System.setOut(originalOut);
            outContent.close();
        }
    }



}