package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.DiscountedItem;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    StandardItem Stanitem;
    DiscountedItem DiscItem;
    @BeforeEach
    void setUp() {
        Stanitem = new StandardItem(1, "testitem", 10.0f, "testcategory", 100);
        DiscItem = new DiscountedItem(2, "disctounteditem", 10.0f, "2category", 100,  new Date(2021), new Date(2022));
    }

    @Test
    void testStandConstructor() {
        ItemStock itemStock = new ItemStock(Stanitem);
        assertEquals(Stanitem, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }
    @Test
    void testDiscConstructor() {
        ItemStock itemStock = new ItemStock(DiscItem);
        assertEquals(DiscItem, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }


    @ParameterizedTest
    @CsvSource({
            "1",
            "2",
            "3",
            " 4",
            "25",
            "0"
    })
    public void testIncreaseItemCount(int increaseBy) {

        StandardItem item2 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);

        ItemStock itemStock = new ItemStock(item2);

        itemStock.IncreaseItemCount(increaseBy);

        assertEquals(increaseBy, itemStock.getCount());
    }
    @ParameterizedTest
    @CsvSource({
            "1",
            "2",
            "3",
            "4",
            "25",
            "0"
    })
    public void testDecreaseItemCount(int decreaseBy) {

        StandardItem item2 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
        ItemStock itemStock = new ItemStock(item2);
        itemStock.IncreaseItemCount(22);

        itemStock.decreaseItemCount(decreaseBy);

        assertEquals(22 - decreaseBy, itemStock.getCount());
    }



}