package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {


    ShoppingCart cart;
    String customerName;
    String customerAddress;
    int state;

    @BeforeEach
    public void setUp() {
        cart = new ShoppingCart();
        customerName = "John Bob";
        customerAddress = "1234 Blby Street";
        state = 1;

    }



    @Test
    void testOrderConstructor() {
        // Assuming a cart with items
        Order order = new Order(cart, customerName, customerAddress, state);
        assertNotNull(order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());



    }

    @Test
    void testOrderConstructorDefaultState() {
        // Test default state value of constructor without state
        Order order = new Order(cart, customerName, customerAddress);
        assertEquals(0, order.getState()); // Default state value is assumed to be 0
    }

    // Testing how the constructor handles a null ShoppingCart
    @Test
    void testOrderWithNullCart() {

        Exception exception = assertThrows(NullPointerException.class, () -> {
            new Order(null, customerName, customerAddress, state);
        });

        String expectedMessage = "Shopping cart cannot be null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    // Testing how constructor handles null for customer name
    @Test
    void testOrderWithNullCustomerName() {

        Order order = new Order(cart, null, customerAddress, state);
        assertNull(order.getCustomerName());
    }

    // Testing how constructor handles null for customer address
    @Test
    void testOrderWithNullCustomerAddress() {
        Order order = new Order(cart, customerName, null, state);
        assertNull(order.getCustomerAddress());
    }
    //Testing how setter for customer name handles null
    @Test
    void testSetCustomerName() {
        Order order = new Order(cart, customerName, customerAddress, state);
        order.setCustomerName(null);
        assertNull(order.getCustomerName());
    }

    //Testing how setter for customer address handles null
    @Test
    void testSetCustomerAddress() {
        Order order = new Order(cart, customerName, customerAddress, state);
        order.setCustomerAddress(null);
        assertNull(order.getCustomerAddress());
    }
    //Testing how setter for Items handles null
    @Test
    void testSetItems() {
        Order order = new Order(cart, customerName, customerAddress, state);
        order.setItems(null);
        assertNull(order.getItems());
    }


    //Testing how setter for state handles negative values
    @Test
    void testSetState() {
        Order order = new Order(cart, customerName, customerAddress, state);
        order.setState(-1);
        assertEquals(-1, order.getState());
    }

}