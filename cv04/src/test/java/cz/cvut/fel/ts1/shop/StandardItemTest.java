package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class StandardItemTest {

      StandardItem item1;

    @BeforeEach
    public void setUp() {
        item1 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
    }


    @Test
    void testConstructor() {
        assertEquals(1, item1.getID());
        assertEquals("Item1", item1.getName());
        assertEquals(10.0f, item1.getPrice());
        assertEquals("Category1", item1.getCategory());
        assertEquals(100, item1.getLoyaltyPoints());
    }


    @Test
    void testCopy() {
        StandardItem itemCopy = item1.copy();
        assertEquals(item1.getID(), itemCopy.getID());
        assertEquals(item1.getName(), itemCopy.getName());
        assertEquals(item1.getPrice(), itemCopy.getPrice());
        assertEquals(item1.getCategory(), itemCopy.getCategory());
        assertEquals(item1.getLoyaltyPoints(), itemCopy.getLoyaltyPoints());

        // Modifying the copy and checking if the original remains unchanged
        itemCopy.setLoyaltyPoints(200);
        assertEquals(100, item1.getLoyaltyPoints());
    }

    static Stream<Arguments> equalityTestCases() {

        StandardItem item1 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
        StandardItem item2 = new StandardItem(1, "Item1", 10.0f, "Category1", 100);
        StandardItem item3 = new StandardItem(2, "Item2", 20.0f, "Category2", 200);

        StandardItem item4 = new StandardItem(1, "Item1", 10.0f, "Category3", 10);
        StandardItem item5 = new StandardItem(1, "Item2", 20.0f, "Category1", 150);

        return Stream.of(
                Arguments.of(true, item1, item2),
                Arguments.of(false, item1, item3),
                Arguments.of(false, item1, item4),
                Arguments.of(false, item1, item5),
                Arguments.of(true, item1, item1)
        );
    }

    @ParameterizedTest
    @MethodSource("equalityTestCases")
    public void testEquals(boolean expectedResult, StandardItem item1, StandardItem item2) {
        assertEquals(expectedResult, item1.equals(item2));
    }


}